# Creating a Freestyle Job
---

## Preparation

 - Access to your infrastructure server
```
ssh "<your-infrastructure-server-ip>"
```

 - Please install the following packges, by inserting the following commands in the terminal:
 ```
wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
```
```
sudo dpkg -i packages-microsoft-prod.deb
```
```
sudo apt-get update
```
```
sudo apt-get install apt-transport-https -y
```
```
sudo apt-get update
```
```
sudo apt-get install dotnet-sdk-3.1 -y
```
- Verify that the dotnet-sdk was installed succesfully
```
dotnet --list-sdks
```

## Instructions

 - Browse from the **browser** to the jenkins portal:

```
http://<Infrastructure-Server-Ip>:8080
```
 - Create a new job by click "New Item"
 
![Image 2](images/image-02.png)


 - Set the job name to "freestyle-*your-name*" and select "Freestyle project"

![Image 3](images/image-03.png)


 - Configure the job to run exclusively on your Slave node, like so:

![Image 4](images/image-04.png)


 - Configure the job to delete the workspace after each job running, like so:

![Image 13](images/image-13.png)


 - Set the sources repository (git)
 
```
https://gitlab.com/sela-devops/courses/jenkins-cicd/lab-02-code.git
```

![Image 5](images/image-05.png)


 - Configure the build process by adding Shell commands excutables
 
![Image 10](images/pic3.png)

 - Add a Dotnet Restore phase which downloads all the software dependencies, like so:
 
```
dotnet restore "dotnetcore-sample.sln"
```

![Image 6](images/image-06.png)

 - Add antoher "Excute Shell" of Dotnet Build phase which builds the software solutions, like so:
 
```
dotnet build "dotnetcore-sample.sln" --configuration Release
```

 - Add antoher "Excute Shell" of Dotnet Test phase which tests the software solutions for problems, like so:
 
```
dotnet test "dotnetcore-sample.sln"  --configuration Release
```

 - Add antoher "Excute Shell" of Dotnet Publish phase which publishs the final software solutions to a directory, like so:
 
```
dotnet publish "dotnetcore-sample.sln" --configuration Release --output ./artifact
```
 - Add antoher "Excute Shell" of zipping the solution to a Gzip file, like so:

```
tar -zcvf DotNetArtifact.tar.gz artifact/
```

 - Configure a **post build action in order to archive the build artifact (DotNetArtifact.tar.gz)

![Image 7](images/image-07.png)

- Set the job configuration with the following line:
```
*.gz
```

 - Save the changes and click "Build Now"

![Image 8](images/image-08.png)


 - Inspect the build details

![Image 9](images/image-09.png)


 - Inspect the build artifacts and the build console

![Image 10](images/image-10.png)

### Create a view for your freestyle project
 
  - In the jenkins portal click on **New view**
 
![Image 21](images/New_view.PNG)

 - Enter **freestyle** as name, select **List View** and click on **ok** button
 
![Image 20](images/freestyle-1.PNG)

 - Check your freestyle project and click on OK

![Image 1](images/image32.PNG)

 - Please check that your project is in the **freestyle** view

![Image 1](images/image33.PNG)
